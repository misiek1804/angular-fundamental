import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  @Input() netTotalValue: number = 0;
  @Input() totalQuantity: number = 0;
  @Output() showGrossValue: EventEmitter<boolean> = new EventEmitter<boolean>();

  grossTotalValue: number = 0;
  showGrossTotalValue: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  emitShowGrossValue() {
    this.showGrossValue.emit(true);
  }

}
