import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from '../models/products';
import { SummaryComponent } from '../summary/summary.component';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  products: Product[]= [];

  constructor() { 
    this.products = [
      {
        id: 0,
        mark: 'Samsung',
        model: 'Galaxy s8',
        price: 3200,
        quantity: 30,
        localization: 'A-10',
        minNumber: 5
      },
      {
        id: 1,
        mark: 'iPhone',
        model: '8 plus',
        price: 3900,
        quantity: 22,
        localization: 'B-3',
        minNumber: 3
      },
      {
        id: 2,
        mark: 'LG',
        model: 'G4',
        price: 1800,
        quantity: 5,
        localization: 'C-11',
        minNumber: 10
      }
    ]
  }

  ngOnInit() {
  }

  totalNetValue: number = 0;
  @ViewChild(SummaryComponent) summaryRef: SummaryComponent;

  countNetTotalValue(){
    this.totalNetValue = this.products.map(product => product.price * product.quantity).reduce((prev,next)=>prev + next);
    return this.totalNetValue;
  }

  countTotalQuantity() {
    return this.products.map(product => product.quantity).reduce((prev,next)=>prev + next);
  }

  countGrossTotalValue(value) {
    this.summaryRef.grossTotalValue = this.totalNetValue * 1.23;
    this.summaryRef.showGrossTotalValue = true;
  }



}
