export interface Product {
    id: number;
    mark: string;
    model: string;
    price: number;
    quantity: number;
    localization: string;
    minNumber: number;
}