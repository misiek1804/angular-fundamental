import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsListComponent } from './products-list/products-list.component';
import { SummaryComponent } from './summary/summary.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ProductsListComponent, SummaryComponent],
  exports: [ProductsListComponent]
})
export class ProductsModule { }
